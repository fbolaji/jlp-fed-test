import styles from './product-list-item.module.scss'
import Link from 'next/Link'
const ProductListItem = ({ productId, image, price, description }) => {
  return (
    <Link
      key={productId}
      href={{
        pathname: '/product-detail/[id]',
        query: { id: productId },
      }}
    >
      <a className={styles.link}>
        <div className={styles.content}>
          <div>
            <img src={image} alt="" style={{ width: '100%' }} />
          </div>
          <div>{description}</div>
          <div className={styles.price}>&pound;{price}</div>
        </div>
      </a>
    </Link>
  )
}

export default ProductListItem
