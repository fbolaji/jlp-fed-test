import { render, screen } from '@testing-library/react'
import Home from '../pages/index'
import data from '../mockData/data.json'

const props = {
  data,
}

describe('Home', () => {
  it('renders a page heading', () => {
    render(<Home {...props} />)
    const listTitle = screen.getByText('Dishwashers')
    expect(listTitle).toBeInTheDocument()
  })
  it('should display list of product item', () => {
    const listLength = data.products.length
    const expectedlist = [
      {
        productId: '1955287',
        type: 'product',
        title: 'Bosch Serie 2 SMV40C30GB Fully Integrated Dishwasher',
        image: '//johnlewis.scene7.com/is/image/JohnLewis/234378764?',
      },
      {
        productId: '1955287',
        type: 'product',
        title: 'Bosch Serie 2 SMV40C30GB Fully Integrated Dishwasher',
        image: '//johnlewis.scene7.com/is/image/JohnLewis/236888513?',
      },
    ]
    render(<Home {...props} />)
    const images = screen.queryAllByRole('img').length
    const description = 'Bosch SPV4EMX21G Fully Integrated Slimline Dishwasher'
    const getText = screen.getAllByText(description)[0]
    // expect(listLength).toEqual(20)
    // expect(images).toBeEmpty(20)
    expect(getText).toBeInTheDocument()
  })
})
