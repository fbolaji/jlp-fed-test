import { render, screen } from '@testing-library/react'
import ProductDetail from '../pages/product-detail/[id]'
import mockData from '../mockData/data2.json'

const props = {
  data: mockData.detailsData,
}

function getProductById(id) {
  const item = props.data.find((item) => item.productId === id)
  return item
}

describe('Product details', () => {
  it.each(['3218074', '1955287', '5095447', '5029064'])(
    'should display productId: %s info and spec',
    (id) => {
      const item = getProductById(id)
      const attrText = item.details.features[0].attributes
      const productTitle = item.title
      const price = '£' + item.price.now

      render(<ProductDetail data={item} />)

      expect(screen.getByText(item.title)).toBeInTheDocument()
      expect(screen.getByRole('img')).toBeInTheDocument()
      expect(screen.getByText(productTitle)).toBeInTheDocument()
      expect(screen.getByText(price)).toBeInTheDocument()
      expect(screen.getByText('Product information')).toBeInTheDocument()
      expect(
        screen.getAllByText('Product specification')[0],
      ).toBeInTheDocument()
      attrText.forEach((attr) => {
        expect(screen.getAllByText(attr.name)[0]).toBeInTheDocument()
        expect(screen.getAllByText(attr.value)[0]).toBeInTheDocument()
      })
    },
  )
})
