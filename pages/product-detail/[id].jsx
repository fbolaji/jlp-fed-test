import Link from 'next/link'
import ProductCarousel from '../../components/product-carousel/product-carousel'
import styles from './product-detail.module.scss'
import data from '../../mockData/data2.json'

// TODO - uncomment and use getServerSideProps before production build
// export async function getServerSideProps(context) {
//   const id = context.params.id;
//   const response = await fetch(
//     "https://api.johnlewis.com/mobile-apps/api/v1/products/" + id
//   );
//   const data = await response.json();
//
//   return {
//     props: { data },
//   };
// }

export function getStaticPaths() {
  const params = data.detailsData.map((item) => ({
    params: { id: item.productId },
  }))
  return {
    fallback: true,
    paths: [...params],
  }
}

export function getStaticProps(context) {
  const id = context.params.id
  const item = data.detailsData.find((item) => item.productId === id)
  const notFound = item ? false : true

  return {
    props: { data: item },
    notFound,
  }
}

const ProductDetail = ({ data }) => {
  const handleExpand = () => {
    //TODO - read more
  }

  if (data === undefined) return <>Product not found.</>
  const price = '£' + data?.price?.now
  return (
    <div className={styles.gridContainer}>
      <div className={styles.header}>
        <Link
          key="back-btn"
          href={{
            pathname: '/',
          }}
        >
          <a className={styles.btnLink}>
            <span className={styles.hidden}>back to product list</span>
          </a>
        </Link>
        <h1>
          <span dangerouslySetInnerHTML={{ __html: data?.title }} />
        </h1>
      </div>

      <div className="main">
        <ProductCarousel image={data?.media?.images?.urls[0]} />
      </div>

      <div className={styles.aside}>
        <h2>{price}</h2>
        {data?.displaySpecialOffer && <p>{data?.displaySpecialOffer}</p>}
        <p>{data?.additionalServices?.includedServices}</p>
      </div>
      <div className={styles.info}>
        <div>
          <h3>Product information</h3>
          <h6>Product code: {data.code}</h6>
          <div
            dangerouslySetInnerHTML={{
              __html: data?.details?.productInformation,
            }}
          />
        </div>

        <h3>Product specification</h3>
        <ul>
          {data?.details?.features[0]?.attributes.map((item, index) => (
            <li key={'spec-list' + index}>
              <div
                className={styles.col}
                dangerouslySetInnerHTML={{ __html: item?.name }}
              />
              <div
                className={styles.col}
                dangerouslySetInnerHTML={{ __html: item?.value }}
              />
            </li>
          ))}
        </ul>
      </div>
    </div>
  )
}

export default ProductDetail
