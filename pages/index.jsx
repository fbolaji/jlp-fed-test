import Head from 'next/head'
import styles from './index.module.scss'
import mockData from '../mockData/data.json'
import ProductListItem from '../components/product-list-item/product-list-item'

// TODO - uncomment and use getServerSideProps for production build
// export async function getServerSideProps() {
//   const response = await fetch(
//     "https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI"
//   );
//   const data = await response.json();
//   return {
//     props: {
//       data: data,
//     },
//   };
// }

export function getStaticProps() {
  const data = { ...mockData }
  return {
    props: {
      data,
    },
  }
}

const Home = ({ data }) => {
  let items = [...data.products]
  const maxNumberOfList = 20
  return (
    <div>
      <Head>
        <title>JL &amp; Partners | Home</title>
        <meta name="keywords" content="shopping" />
      </Head>
      <div>
        <h1>Dishwashers ({maxNumberOfList})</h1>
        <div className={styles.content}>
          {items.map(
            (item, index) =>
              index < maxNumberOfList && (
                <ProductListItem
                  productId={item.productId}
                  description={item.title}
                  image={item.image}
                  price={item.price.now}
                />
              ),
          )}
        </div>
      </div>
    </div>
  )
}

export default Home
